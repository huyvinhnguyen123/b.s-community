package Blind.Sight.community.web.controller;

import Blind.Sight.community.config.security.JwtUtil;
import Blind.Sight.community.domain.repository.postgresql.UserRepositoryPg;
import Blind.Sight.community.domain.service.write.UserServicePg;
import Blind.Sight.community.dto.user.UserDataInput;
import Blind.Sight.community.web.controller.AuthenticationController;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AuthenticationController.class)
class UserControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private UserServicePg userServicePg;
    @MockBean
    private AuthenticationManager authenticationManager;
    @MockBean
    private JwtUtil jwtUtil;
    @MockBean
    private UserRepositoryPg userRepositoryPg;

    @Test
    void testCreateUser() throws Exception {
        // Arrange
        // user input
        UserDataInput userDataInput = new UserDataInput();
        userDataInput.setName("Diem My");
        userDataInput.setEmail("diemmy@gmail.com");
        userDataInput.setPassword("Diemmy11122000");
        userDataInput.setBirthDate("20001211");

        // uri input
        String uri = "/api/v1/register";

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(new ObjectMapper().writeValueAsString(userDataInput)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message").value("OK"));

        // verify that the userServicePg.createUser method was called with the correct input
        verify(userServicePg, times(1)).createUser(userDataInput);
    }
}
