package Blind.Sight.community.domain.service.write;

import Blind.Sight.community.config.security.JwtUtil;
import Blind.Sight.community.config.security.PasswordEncrypt;
import Blind.Sight.community.domain.entity.Image;
import Blind.Sight.community.domain.entity.User;
import Blind.Sight.community.domain.repository.postgresql.ImageRepositoryPg;
import Blind.Sight.community.domain.repository.postgresql.UserRepositoryPg;
import Blind.Sight.community.domain.service.many.UserImageService;
import Blind.Sight.community.domain.service.system.OtpService;
import Blind.Sight.community.domain.service.write.AdminServicePg;
import Blind.Sight.community.domain.service.write.ImageServicePg;
import Blind.Sight.community.domain.service.write.UserServicePg;
import Blind.Sight.community.dto.user.LoginInput;
import Blind.Sight.community.dto.user.UserDataInput;
import Blind.Sight.community.util.common.RoleData;
import Blind.Sight.community.util.format.CustomDateTimeFormatter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.TestPropertySource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@TestPropertySource(
        locations = "classpath:application-test.properties",
        properties = "drive.folder.users=1LP81qdcHREPYSXP-7HIXFtuLxoHRG7hS"
)
class UserServicePgTest {
    @Mock
    private UserRepositoryPg userRepositoryPg;
    @Mock
    private ImageRepositoryPg imageRepositoryPg;
    @Mock
    private Authentication authentication;
    @Mock
    private AuthenticationManager authenticationManager;
    @Mock
    private JwtUtil jwtUtil;
    @Mock
    private PasswordEncrypt passwordEncrypt;
    @Mock
    private ImageServicePg imageServicePg;
    @Mock
    private UserImageService userImageService;
    @Mock
    private OtpService otpService;
    @InjectMocks
    private AdminServicePg adminServicePg;
    @InjectMocks
    private UserServicePg userServicePg;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

//========================================================================================================================
    // Register test
    // Happy case;
    @Test
    void testCreateSystem() {
        // Arrange
        UserDataInput userDataInput = new UserDataInput();
        userDataInput.setName("SYSTEM BS ADMIN");
        userDataInput.setEmail("system.bs@gmail.com");
        userDataInput.setPassword("systemAKJBUIBLP48138");
        userDataInput.setBirthDate("20240112");

        // this is the result you want to return in actual not the reality result in database
        when(passwordEncrypt.bcryptPasswordTest(anyString())).thenReturn("hashedPassword");

        // Act
        adminServicePg.createSystem(userDataInput);

        ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);
        verify(userRepositoryPg, times(1)).save(userCaptor.capture());

        User capturedUser = userCaptor.getValue();

        // Assert
        assertNotNull(userCaptor);
        assertEquals(userDataInput.getName(), capturedUser.getUsername());
        assertEquals(userDataInput.getEmail(), capturedUser.getEmail());
        assertEquals(CustomDateTimeFormatter.dateOfBirthFormatter(userDataInput.getBirthDate()), capturedUser.getBirthDate());
    }

    @Test
    void testCreateAdmin() {
        // Arrange
        UserDataInput userDataInput = new UserDataInput();
        userDataInput.setName("Lumina");
        userDataInput.setEmail("huy.vinhnguyen@outlook.com");
        userDataInput.setPassword("Huynv123");
        userDataInput.setBirthDate("19981123");

        // this is the result you want to return in actual not the reality result in database
        when(passwordEncrypt.bcryptPasswordTest(anyString())).thenReturn("hashedPassword");

        // Act
        adminServicePg.createAdmin(userDataInput);

        ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);
        verify(userRepositoryPg, times(1)).save(userCaptor.capture());

        User capturedUser = userCaptor.getValue();

        // Assert
        assertNotNull(userCaptor);
        assertEquals(userDataInput.getName(), capturedUser.getUsername());
        assertEquals(userDataInput.getEmail(), capturedUser.getEmail());
        assertEquals(CustomDateTimeFormatter.dateOfBirthFormatter(userDataInput.getBirthDate()), capturedUser.getBirthDate());
    }

    @Test
    void testCreateUserSuccess() {
        // Arrange
        UserDataInput userDataInput = new UserDataInput();
        userDataInput.setName("Diem My");
        userDataInput.setEmail("diemmy@gmail.com");
        userDataInput.setPassword("Diemmy11122000");
        userDataInput.setBirthDate("20001211");

        // this is the result you want to return in actual not the reality result in database
        when(passwordEncrypt.bcryptPasswordTest(anyString())).thenReturn("hashedPassword");

        // Act
        userServicePg.createUser(userDataInput);

        ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);
        verify(userRepositoryPg, times(1)).save(userCaptor.capture());

        User capturedUser = userCaptor.getValue();

        // Assert
        assertNotNull(userCaptor);
        assertEquals(userDataInput.getName(), capturedUser.getUsername());
        assertEquals(userDataInput.getEmail(), capturedUser.getEmail());
        assertEquals(CustomDateTimeFormatter.dateOfBirthFormatter(userDataInput.getBirthDate()), capturedUser.getBirthDate());
    }

    // Sad case;
    @Test
    void testCreateUserFail() {
        // Arrange
        UserDataInput userDataInput = new UserDataInput();
        userDataInput.setName("Diem My");
        userDataInput.setEmail("diemmy@gmail.com");
        userDataInput.setPassword("Diemmy11122000");
        userDataInput.setBirthDate("20001211");

        // simulate a failure in saving the user
        when(passwordEncrypt.bcryptPasswordTest(anyString())).thenReturn("hashedPassword");
        doThrow(new RuntimeException("Failed to save user")).when(userRepositoryPg).save(any(User.class));

        // Act & Assert
        assertThrows(RuntimeException.class, () -> {
            userServicePg.createUser(userDataInput);
        });

        // verify that the save method was called
        verify(userRepositoryPg, times(1)).save(any(User.class));

    }

    // Boundary case;
    @Test
    void testCreateUserEmpty() {
        // Arrange
        UserDataInput userDataInputEmptyName = new UserDataInput();
        userDataInputEmptyName.setName("");
        userDataInputEmptyName.setEmail("diemmy@gmail.com");
        userDataInputEmptyName.setPassword("Diemmy11122000");
        userDataInputEmptyName.setBirthDate("20001211");

        UserDataInput userDataInputEmptyEmail = new UserDataInput();
        userDataInputEmptyEmail.setName("Diem My");
        userDataInputEmptyEmail.setEmail("");
        userDataInputEmptyEmail.setPassword("Diemmy11122000");
        userDataInputEmptyEmail.setBirthDate("20001211");

        UserDataInput userDataInputEmptyPassword = new UserDataInput();
        userDataInputEmptyPassword.setName("Diem My");
        userDataInputEmptyPassword.setEmail("diemmy@gmail.com");
        userDataInputEmptyPassword.setPassword("");
        userDataInputEmptyPassword.setBirthDate("20001211");

        UserDataInput userDataInputEmptyBirthdate = new UserDataInput();
        userDataInputEmptyBirthdate.setName("Diem My");
        userDataInputEmptyBirthdate.setEmail("diemmy@gmail.com");
        userDataInputEmptyBirthdate.setPassword("");
        userDataInputEmptyBirthdate.setBirthDate("20001211");

        // simulate a failure in saving the user
        when(passwordEncrypt.bcryptPasswordTest(anyString())).thenReturn("hashedPassword");
        doThrow(new NullPointerException("Failed to save user")).when(userRepositoryPg).save(any(User.class));

        // Act & Assert
        assertThrows(NullPointerException.class, () -> {
            userServicePg.createUser(userDataInputEmptyName);
        });

        assertThrows(NullPointerException.class, () -> {
            userServicePg.createUser(userDataInputEmptyEmail);
        });

        assertThrows(NullPointerException.class, () -> {
            userServicePg.createUser(userDataInputEmptyPassword);
        });

        assertThrows(NullPointerException.class, () -> {
            userServicePg.createUser(userDataInputEmptyBirthdate);
        });

        // verify that the save method was called
        verify(userRepositoryPg, times(4)).save(any(User.class));
    }

    @Test
    void testCreateUserInvalidCharacter() {
        // Arrange
        UserDataInput userDataInputInvalidName = new UserDataInput();
        userDataInputInvalidName.setName("1DiemMy");
        userDataInputInvalidName.setEmail("diemmy@gmail.com");
        userDataInputInvalidName.setPassword("Diemmy11122000");
        userDataInputInvalidName.setBirthDate("20001211");

        UserDataInput userDataInputInvalidEmail = new UserDataInput();
        userDataInputInvalidEmail.setName("Diem My");
        userDataInputInvalidEmail.setEmail("diemmy#gmail.com");
        userDataInputInvalidEmail.setPassword("Diemmy11122000");
        userDataInputInvalidEmail.setBirthDate("20001211");

        UserDataInput userDataInputInvalidPassword = new UserDataInput();
        userDataInputInvalidPassword.setName("Diem My");
        userDataInputInvalidPassword.setEmail("diemmy@gmail.com");
        userDataInputInvalidPassword.setPassword("11122000");
        userDataInputInvalidPassword.setBirthDate("20001211");

        UserDataInput userDataInputInvalidBirthdate = new UserDataInput();
        userDataInputInvalidBirthdate.setName("Diem My");
        userDataInputInvalidBirthdate.setEmail("diemmy@gmail.com");
        userDataInputInvalidBirthdate.setPassword("Diemmy11122000");
        userDataInputInvalidBirthdate.setBirthDate("2000-12-11");

        // simulate a failure in saving the user
        when(passwordEncrypt.bcryptPasswordTest(anyString())).thenReturn("hashedPassword");
        doThrow(new RuntimeException("Failed to save user")).when(userRepositoryPg).save(any(User.class));

        // Act & Assert
        assertThrows(RuntimeException.class, () -> {
            userServicePg.createUser(userDataInputInvalidName);
        });

        assertThrows(RuntimeException.class, () -> {
            userServicePg.createUser(userDataInputInvalidEmail);
        });

        assertThrows(RuntimeException.class, () -> {
            userServicePg.createUser(userDataInputInvalidPassword);
        });

        assertThrows(RuntimeException.class, () -> {
            userServicePg.createUser(userDataInputInvalidBirthdate);
        });

        // verify that the save method was called
        verify(userRepositoryPg, times(3)).save(any(User.class));
    }

    @Test
    void testCreateUserMaxLength() {
        // Arrange
        UserDataInput userDataInputMaxLength= new UserDataInput();
        userDataInputMaxLength.setName("a".repeat(255));
        userDataInputMaxLength.setEmail("a".repeat(255) + "@gmail.com");
        userDataInputMaxLength.setPassword("a".repeat(255) + "A12");
        userDataInputMaxLength.setBirthDate("20001211");

        // simulate a failure in saving the user
        when(passwordEncrypt.bcryptPasswordTest(anyString())).thenReturn("hashedPassword");
        doThrow(new RuntimeException("Failed to save user")).when(userRepositoryPg).save(any(User.class));

        // Act & Assert
        assertDoesNotThrow(() -> {
            assertNotNull(userDataInputMaxLength.getName());
            assertNotNull(userDataInputMaxLength.getBirthDate());
            assertNotNull(userDataInputMaxLength.getEmail());
            assertNotNull(userDataInputMaxLength.getPassword());
        });

        assertThrows(RuntimeException.class, () ->
                userServicePg.createUser(userDataInputMaxLength));
    }
//========================================================================================================================
    // Find by id test
    // Happy case;
//    @Test
//    void testFindUserByIdSuccess() {
//        // Arrange
//        String userId = "af7c1fe6-d669-414e-b066-e9733f0de7a8";
//        User mockedUser = new User("Diem My", "diemmy@gmail.com", RoleData.USER.getRole());
//        mockedUser.setBirthDate(CustomDateTimeFormatter.dateOfBirthFormatter("20011211"));
//        mockedUser.setPassword(PasswordEncrypt.bcryptPassword("11122000"));
//
//        // this is the result you want to return in actual not the reality result in database
//        when(userRepositoryPg.findById(userId)).thenReturn(Optional.of(mockedUser));
//
//        // Act
//        User resultUser = userServicePg.findUserById(userId);
//
//        verify(userRepositoryPg, times(1)).findById(userId);
//
//        // Assert
//        assertNotNull(resultUser);
//        assertEquals("Diem My", resultUser.getUsername());
//        assertEquals("diemmy@gmail.com", resultUser.getEmail());
//    }

    // Sad case;
    @Test
    void testFindUserByIdFail() {
        // Arrange
        String userId = "af7c1fe6-d669-414e-b066-e9733f0de7a8";

        // this is the result you want to return in actual not the reality result in database
        when(userRepositoryPg.findById(userId)).thenReturn(null);

        // Act & Assert
        assertThrows(NullPointerException.class,
                () -> userServicePg.findUserById(userId));
    }
//========================================================================================================================
    // Login test;
    // Happy case;
    @Test
    void testLoginSuccess() {
        // Arrange
        // login input
        LoginInput loginInput = new LoginInput();
        loginInput.setEmail("huy.vinhnguyen@outlook.com");
        loginInput.setPassword("huyNV123");

        // assuming you have a User class
        Authentication mockedAuthentication = mock(Authentication.class);
        User userMock = new User();

        // this is the result you want to return in actual not the reality result in database
        when(mockedAuthentication.getPrincipal()).thenReturn(userMock);
        when(authenticationManager.authenticate(any(UsernamePasswordAuthenticationToken.class))).thenReturn(mockedAuthentication);
        when(jwtUtil.createToken(any(User.class))).thenReturn("mockedToken");

        // Act
        String resultToken = userServicePg.login(loginInput, authenticationManager, jwtUtil);

        // Assert
        verify(authenticationManager, times(1)).authenticate(any(UsernamePasswordAuthenticationToken.class));
        verify(jwtUtil, times(1)).createToken(userMock);
        assertEquals("mockedToken", resultToken);
    }

    // Sad case;
    @Test
    void testLoginFail() {
        // Arrange
        // login input
        LoginInput loginInput = new LoginInput();
        loginInput.setEmail("test.account@example.com");
        loginInput.setPassword("testAccount123");

        // this is the result you want to return in actual not the reality result in database
        when(authenticationManager.authenticate(any(UsernamePasswordAuthenticationToken.class)))
                .thenThrow(new BadCredentialsException("Authentication failed."));

        // Act & Assert
        assertThrows(BadCredentialsException.class,
                () -> userServicePg.login(loginInput, authenticationManager, jwtUtil));
    }

    // Boundary case;
    @Test
    void testLoginEmpty() {
        // Arrange
        // login input empty email
        LoginInput loginInputEmptyEmail = new LoginInput();
        loginInputEmptyEmail.setEmail("");
        loginInputEmptyEmail.setPassword("testAccount123");

        // login input empty password
        LoginInput loginInputEmptyPassword = new LoginInput();
        loginInputEmptyPassword.setEmail("test.account@example.com");
        loginInputEmptyPassword.setPassword("");

        // login input null email
        LoginInput loginInputNullEmail = new LoginInput();
        loginInputEmptyEmail.setPassword("testAccount123");
        loginInputNullEmail.setEmail(null);

        // login input null password
        LoginInput loginInputNullPassword = new LoginInput();
        loginInputEmptyPassword.setEmail("test.account@example.com");
        loginInputNullPassword.setPassword(null);

        // Act & Assert
        assertThrows(NullPointerException.class, () ->
                userServicePg.login(loginInputEmptyEmail, authenticationManager, jwtUtil));

        assertThrows(NullPointerException.class, () ->
                userServicePg.login(loginInputEmptyPassword, authenticationManager, jwtUtil));

        assertThrows(NullPointerException.class, () ->
                userServicePg.login(loginInputNullEmail, authenticationManager, jwtUtil));

        assertThrows(NullPointerException.class, () ->
                userServicePg.login(loginInputNullPassword, authenticationManager, jwtUtil));
    }

    @Test
    void testLoginMaxLength() {
        // Arrange
        String maxLengthEmail = "a".repeat(255) + "@gmail.com";
        String maxLengthPassword = "a".repeat(255);

        LoginInput loginInput = new LoginInput();
        loginInput.setEmail(maxLengthEmail);
        loginInput.setPassword(maxLengthPassword);

        // Act & Assert
        assertDoesNotThrow(() -> {
            assertNotNull(loginInput.getEmail());
            assertNotNull(loginInput.getPassword());
        });

        assertThrows(NullPointerException.class, () ->
                userServicePg.login(loginInput, authenticationManager, jwtUtil));
    }

    @Test
    void testLoginInvalidCharacters() {
        // Arrange
        // login input
        LoginInput invalidEmailInput = new LoginInput();
        invalidEmailInput.setEmail("test.accountexample");
        invalidEmailInput.setPassword("testAccount123");

        LoginInput invalidPasswordInput = new LoginInput();
        invalidPasswordInput.setEmail("test.account@example.com");
        invalidPasswordInput.setPassword("123");

        assertThrows(NullPointerException.class, () ->
                userServicePg.login(invalidEmailInput, authenticationManager, jwtUtil));

        assertThrows(NullPointerException.class, () ->
                userServicePg.login(invalidPasswordInput, authenticationManager, jwtUtil));

    }
//========================================================================================================================
    // Update test
    // Happy case;
//    @Test
//    void testUpdateUserAndCreateImageSuccess() throws GeneralSecurityException, IOException {
//        // Arrange
//        // user input
//        UserDataInput userDataInput = new UserDataInput();
//        userDataInput.setName("Diem My");
//        userDataInput.setEmail("diemmy@gmail.com");
//        userDataInput.setPassword("Diemmy11122000");
//        userDataInput.setBirthDate("20001211");
//        // Load an actual image file from resources
//        Path imagePath = Paths.get("D:/file/user/One_leg_up_web.jpg");
//        byte[] imageBytes = Files.readAllBytes(imagePath);
//        userDataInput.setFile(new MockMultipartFile("image", "One_leg_up_web.jpg", "image/jpg", imageBytes));
//
//        // user output
//        String userId = "af7c1fe6-d669-414e-b066-e9733f0de7a8";
//        User mockedUser = new User("Diem My", "diemmy@gmail.com", RoleData.USER.getRole());
//        mockedUser.setBirthDate(CustomDateTimeFormatter.dateOfBirthFormatter("20011211"));
//        mockedUser.setPassword(PasswordEncrypt.bcryptPassword("11122000"));
//
//        // this is the result you want to return in actual not the reality result in database
//        when(authentication.isAuthenticated()).thenReturn(true);
//        when(authentication.getPrincipal()).thenReturn(mockedUser);
//        when(passwordEncrypt.bcryptPasswordTest(anyString())).thenReturn("hashedPassword");
//        when(userRepositoryPg.findById(userId)).thenReturn(Optional.of(mockedUser));
//
//        // Act
//        // Sub act: createImage(), updateImage(), createUserImage()
//        // Sub act updateImage(): findImageById()
//        userServicePg.updateStagingUserAndImage(authentication, userId, userDataInput);
//
//        ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);
//        verify(userRepositoryPg, times(1)).save(userCaptor.capture());
//
//        ArgumentCaptor<MultipartFile> fileCaptor = ArgumentCaptor.forClass(MultipartFile.class);
//        ArgumentCaptor<String> pathCaptor = ArgumentCaptor.forClass(String.class);
//        ArgumentCaptor<String> categoryCaptor = ArgumentCaptor.forClass(String.class);
//        verify(imageServicePg, times(1)).createImage(fileCaptor.capture(), pathCaptor.capture(), categoryCaptor.capture());
//
//        ArgumentCaptor<Image> imageCaptor = ArgumentCaptor.forClass(Image.class);
//        verify(userImageService, times(1)).createUserImage(userCaptor.capture(), imageCaptor.capture());
//
//        User capturedUser = userCaptor.getValue();
//
//        // Assert
//        assertNotNull(userCaptor);
//        assertEquals(userDataInput.getName(), capturedUser.getUsername());
//        assertEquals(userDataInput.getEmail(), capturedUser.getEmail());
//        assertEquals(CustomDateTimeFormatter.dateOfBirthFormatter(userDataInput.getBirthDate()), capturedUser.getBirthDate());
//
//        assertNotNull(fileCaptor);
//        assertEquals(userDataInput.getFile(), fileCaptor.getValue());
//        assertEquals("1LP81qdcHREPYSXP-7HIXFtuLxoHRG7hS", pathCaptor.getValue());
//        assertEquals("user", categoryCaptor.getValue());
//    }

    // Sad case;
//    @Test
//    void testUpdateUserAndCreateImageFailure() throws GeneralSecurityException, IOException {
//        // Arrange
//        // user input
//        UserDataInput userDataInput = new UserDataInput();
//        userDataInput.setName("Diem My");
//        userDataInput.setEmail("diemmy@gmail.com");
//        userDataInput.setPassword("Diemmy11122000");
//        userDataInput.setBirthDate("20001211");
//
//        // user output
//        String userId = "af7c1fe6-d669-414e-b066-e9733f0de7a8";
//        User mockedUser = new User("Diem My", "diemmy@gmail.com", RoleData.USER.getRole());
//        mockedUser.setBirthDate(CustomDateTimeFormatter.dateOfBirthFormatter("20011211"));
//        mockedUser.setPassword(PasswordEncrypt.bcryptPassword("11122000"));
//
//        // this is the result you want to return in actual not the reality result in database
//        when(authentication.isAuthenticated()).thenReturn(true);
//        when(authentication.getPrincipal()).thenReturn(mockedUser);
//        when(passwordEncrypt.bcryptPasswordTest(anyString())).thenReturn("hashedPassword");
//        when(userRepositoryPg.findById(userId)).thenReturn(Optional.of(mockedUser));
//
//        // Act
//        // Sub act: createImage(), updateImage(), createUserImage()
//        // Sub act updateImage(): findImageById()
//        userServicePg.updateStagingUserAndImage(authentication, userId, userDataInput);
//
//        ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);
//        verify(userRepositoryPg, times(1)).save(userCaptor.capture());
//
//        ArgumentCaptor<Image> imageCaptor = ArgumentCaptor.forClass(Image.class);
//        verify(imageRepositoryPg, never()).save(imageCaptor.capture());
//    }
//========================================================================================================================
    // Find image by id
    // Happy case;
    @Test
    void testFindImageByIdSuccess() {
        // Arrange
        String imageId = "Image-77c7a37c99";
        Image mockedImage = new Image("1CriQ521447dETd0617rv5rj5P14VxZ1G",
                "13a2441a9228fb49ed284279c6874a17.jpg",
                "https://drive.google.com/uc?id=1TuZvjz5XQSkrCQGTRKCJeE_zIoR8_pEt&export=download");

        // this is the result you want to return in actual not the reality result in database
        when(imageServicePg.findImageById(imageId)).thenReturn(mockedImage);

        // Act
        Image resultImage = imageServicePg.findImageById(imageId);

        verify(imageServicePg, times(1)).findImageById(imageId);

        // Assert
        assertNotNull(resultImage);
        assertEquals("13a2441a9228fb49ed284279c6874a17.jpg", resultImage.getImageName());
    }

    // Sad case;
    @Test
    void testFindImageByIdFailure() {
        // Arrange
        String imageId = "Image-77c7a37c99";

        // this is the result you want to return in actual not the reality result in database
        when(imageServicePg.findImageById(imageId)).thenReturn(null);

        // Act
        Image resultImage = imageServicePg.findImageById(imageId);

        verify(imageServicePg, times(1)).findImageById(imageId);

        // Assert
        assertNull(resultImage);
    }
//========================================================================================================================
    // Happy case;
//    @Test
//    void testUpdateUserAndDeleteImage() throws GeneralSecurityException, IOException {
//        // Arrange
//        // user output
//        String userId = "af7c1fe6-d669-414e-b066-e9733f0de7a8";
//        User mockedUser = new User("Diem My", "diemmy@gmail.com", RoleData.USER.getRole());
//        mockedUser.setBirthDate(CustomDateTimeFormatter.dateOfBirthFormatter("20011211"));
//        mockedUser.setPassword(PasswordEncrypt.bcryptPassword("11122000"));
//
//        // image output
//        String imageId = "Image-77c7a37c99";
//        Image mockedImage = new Image("1CriQ521447dETd0617rv5rj5P14VxZ1G",
//                "13a2441a9228fb49ed284279c6874a17.jpg",
//                "https://drive.google.com/uc?id=1TuZvjz5XQSkrCQGTRKCJeE_zIoR8_pEt&export=download");
//
//        // this is the result you want to return in actual not the reality result in database
//        // mocked (repository)
//        when(userRepositoryPg.findById(userId)).thenReturn(Optional.of(mockedUser));
//        // inject mocked (service, other,...)
//        when(authentication.isAuthenticated()).thenReturn(true);
//        when(authentication.getPrincipal()).thenReturn(mockedUser);
//        when(passwordEncrypt.bcryptPasswordTest(anyString())).thenReturn("hashedPassword");
//        when(imageServicePg.findImageById(imageId)).thenReturn(mockedImage);
//
//        // Act
//        // Sub act: deleteImage()
//        // Sub act deleteImage(): findImageById()
//        userServicePg.updateUserAndDeleteImage(authentication, userId, imageId);
//        imageServicePg.findImageById(imageId);
//        imageServicePg.deleteImage(imageId);
//
//        verify(imageServicePg, times(1)).findImageById(imageId);
//        verify(imageServicePg, times(1)).deleteImage(imageId);
//    }

    // Happy case;
//    @Test
//    void testUpdateUserAndUpdateImage() throws GeneralSecurityException, IOException {
//        // Arrange
//        // user input
//        UserDataInput userDataInput = new UserDataInput();
//        userDataInput.setName("Diem My");
//        userDataInput.setEmail("diemmy@gmail.com");
//        userDataInput.setPassword("Diemmy11122000");
//        userDataInput.setBirthDate("20001211");
//        // Load an actual image file from resources
//        Path imagePath = Paths.get("D:/file/user/One_leg_up_web.jpg");
//        byte[] imageBytes = Files.readAllBytes(imagePath);
//        userDataInput.setFile(new MockMultipartFile("image", "One_leg_up_web.jpg", "image/jpg", imageBytes));
//
//        // user output
//        String userId = "af7c1fe6-d669-414e-b066-e9733f0de7a8";
//        User mockedUser = new User("Diem My", "diemmy@gmail.com", RoleData.USER.getRole());
//        mockedUser.setBirthDate(CustomDateTimeFormatter.dateOfBirthFormatter("20011211"));
//        mockedUser.setPassword(PasswordEncrypt.bcryptPassword("11122000"));
//
//        // image output
//        String imageId = "Image-77c7a37c99";
//        Image mockedImage = new Image("1CriQ521447dETd0617rv5rj5P14VxZ1G",
//                "13a2441a9228fb49ed284279c6874a17.jpg",
//                "https://drive.google.com/uc?id=1TuZvjz5XQSkrCQGTRKCJeE_zIoR8_pEt&export=download");
//
//        // this is the result you want to return in actual not the reality result in database
//        when(authentication.isAuthenticated()).thenReturn(true);
//        when(authentication.getPrincipal()).thenReturn(mockedUser);
//        when(passwordEncrypt.bcryptPasswordTest(anyString())).thenReturn("hashedPassword");
//        when(userRepositoryPg.findById(userId)).thenReturn(Optional.of(mockedUser));
//        when(imageRepositoryPg.findById(imageId)).thenReturn(Optional.of(mockedImage));
//
//        // Act
//        // Sub act: createImage(), updateImage(), createUserImage()
//        // Sub act updateImage(): findImageById()
//        userServicePg.updateStagingUserAndImage(authentication, userId, userDataInput);
//        imageServicePg.updateReplaceImage(imageId, userDataInput.getFile(), "1LP81qdcHREPYSXP-7HIXFtuLxoHRG7hS");
//
//        ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);
//        verify(userRepositoryPg, times(1)).save(userCaptor.capture());
//
//        ArgumentCaptor<MultipartFile> fileCaptor = ArgumentCaptor.forClass(MultipartFile.class);
//        ArgumentCaptor<String> pathCaptor = ArgumentCaptor.forClass(String.class);
//        verify(imageServicePg, times(1)).updateReplaceImage(eq(imageId), fileCaptor.capture(), pathCaptor.capture());
//
//        User capturedUser = userCaptor.getValue();
//
//        // Assert
//        assertNotNull(userCaptor);
//        assertEquals(userDataInput.getName(), capturedUser.getUsername());
//        assertEquals(userDataInput.getEmail(), capturedUser.getEmail());
//        assertEquals(CustomDateTimeFormatter.dateOfBirthFormatter(userDataInput.getBirthDate()), capturedUser.getBirthDate());
//
//        assertNotNull(fileCaptor);
//        assertEquals(userDataInput.getFile(), fileCaptor.getValue());
//        assertEquals("1LP81qdcHREPYSXP-7HIXFtuLxoHRG7hS", pathCaptor.getValue());
//    }

    // Happy case;
//    @Test
//    void testSendMailResetPassword() {
//        // Arrange
//        String email = "huy.vinhnguyen@outlook.com";
//        User mockedUser = new User("Diem My", "diemmy@gmail.com", RoleData.USER.getRole());
//        mockedUser.setBirthDate(CustomDateTimeFormatter.dateOfBirthFormatter("20011211"));
//        mockedUser.setPassword(PasswordEncrypt.bcryptPassword("11122000"));
//
//        // this is the result you want to return in actual not the reality result in database
//        when(userRepositoryPg.findUserByEmail(email)).thenReturn(Optional.of(mockedUser));
//
//        // Act
//        userServicePg.sendMailResetPassword(email);
//
//        verify(otpService, times(1)).generateOtp(email);
//
//    }
}






