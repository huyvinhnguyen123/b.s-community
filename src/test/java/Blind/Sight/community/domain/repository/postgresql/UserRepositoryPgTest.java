package Blind.Sight.community.domain.repository.postgresql;

import Blind.Sight.community.config.database.PostGreSQLConfig;
import Blind.Sight.community.config.security.PasswordEncrypt;
import Blind.Sight.community.domain.entity.User;
import Blind.Sight.community.domain.repository.postgresql.UserRepositoryPg;
import Blind.Sight.community.util.common.RoleData;
import Blind.Sight.community.util.format.CustomDateTimeFormatter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@DataJpaTest
@ContextConfiguration(classes = {PostGreSQLConfig.class})
@ExtendWith(SpringExtension.class)
@TestPropertySource(
        locations = "classpath:application-test.properties"
)
class UserRepositoryPgTest {
    @Autowired
    private UserRepositoryPg userRepositoryPg;

//    @Test
//    void testCreateUser() {
//        User user = new User("john", "john@gmail.com", RoleData.USER.getRole());
//        user.setBirthDate(CustomDateTimeFormatter.dateOfBirthFormatter("20011211"));
//        user.setPassword(PasswordEncrypt.bcryptPassword("11122000"));
//
//        User savedUser = userRepositoryPg.save(user);
//
//        Assertions.assertNotNull(savedUser);
//    }
}
