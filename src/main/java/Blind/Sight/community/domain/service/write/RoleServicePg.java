package Blind.Sight.community.domain.service.write;

import Blind.Sight.community.domain.entity.Role;
import Blind.Sight.community.domain.entity.User;
import Blind.Sight.community.domain.repository.postgresql.RoleRepositoryPg;
import Blind.Sight.community.domain.repository.postgresql.UserRepositoryPg;
import Blind.Sight.community.util.common.RoleData;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Slf4j
@RequiredArgsConstructor
public class RoleServicePg {
    private final RoleRepositoryPg roleRepositoryPg;
    private final UserRepositoryPg userRepositoryPg;

    public Role findRoleById(Integer roleId) {
        Role existRole = roleRepositoryPg.findById(roleId).orElseThrow(
                () -> {
                    log.error("Not found this id: {}", roleId);
                    return new NullPointerException("Not found this id: " + roleId);
                }
        );

        log.info("Found role");
        return existRole;
    }

    public User findUserById(User user) {
        User existUser = userRepositoryPg.findById(user.getUserId()).orElseThrow(
                () -> {
                    log.error("Not found this id: {}", user.getUserId());
                    return new NullPointerException("Not found this id: " + user.getUserId());
                }
        );

        log.info("Found user");
        return existUser;
    }

    public Role addRole(User user, RoleData roleData) {
        User existUser = findUserById(user);
        Role role = new Role();
        role.setRoleName(roleData.getRole());
        role.setUser(existUser);

        if (role.getRoleName().equals("ROLE_ADMIN") || role.getRoleName().equals("ROLE_SYSTEM")) {
            role.setIsAdministration(true);
        }else{
            role.setIsAdministration(false);
        }

        roleRepositoryPg.save(role);
        log.info("Create role success");

        return role;
    }

    public Set<Role> addRoles(User user, RoleData roleData) {
        Set<Role> roles = new HashSet<>();
        roles.add(addRole(user, roleData));
        return roles;
    }

    public void updateRole(User user, Role role, RoleData roleData) {
        User existUser = findUserById(user);
        Role existRole = findRoleById(role.getRoleId());

        if(existRole.getUser().getUserId().equals(existUser.getUserId())) {
            existRole.setRoleName(roleData.getRole());
            existRole.setDescription(role.getDescription());
            if(existRole.getRoleName().equals("ROLE_ADMIN") || existRole.getRoleName().equals("ROLE_SYSTEM")) {
                existRole.setIsAdministration(true);
            }
        }

        roleRepositoryPg.save(existRole);
        log.info("Update role success");
    }

}
