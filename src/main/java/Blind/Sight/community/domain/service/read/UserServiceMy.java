package Blind.Sight.community.domain.service.read;

import Blind.Sight.community.domain.repository.mysql.UserRepositoryMy;
import Blind.Sight.community.domain.repository.mysql.object.UserDataForSearching;
import Blind.Sight.community.dto.user.UserData;
import Blind.Sight.community.dto.user.UserDataForAdmin;
import Blind.Sight.community.dto.user.UserDataForUser;
import Blind.Sight.community.dto.user.UserSearchInput;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceMy {
    private final UserRepositoryMy userRepositoryMy;

    public Page<UserData> findAllUsers(String role, Pageable pageable) {
        Page<UserDataForSearching> users = userRepositoryMy.findAllUsers(role, pageable);

        return users.map(user -> {
            UserData userData = new UserData();
            userData.setId(user.getUserId());
            userData.setBirthDate(user.getBirthDate());
            userData.setName(user.getUsername());
            userData.setEmail(user.getEmail());
            userData.setRole(role);

            log.info("Select user success");
            return userData;
        });
    }

    public Page<UserDataForAdmin> findAllUsersWithImage(String role, Pageable pageable) {
        Page<UserDataForSearching> users = userRepositoryMy.findAllUsersWithImage(role, pageable);

        return users.map(user -> {
            UserDataForAdmin userDataForAdmin = new UserDataForAdmin();
            userDataForAdmin.setId(user.getUserId());
            userDataForAdmin.setName(user.getUsername());
            userDataForAdmin.setBirthDate(user.getBirthDate());
            userDataForAdmin.setEmail(user.getEmail());
            userDataForAdmin.setRole(user.getRole());
            userDataForAdmin.setImageId(user.getImageId());
            userDataForAdmin.setImageName(user.getImageName());
            userDataForAdmin.setImagePath(user.getImagePath());
            log.info("Select user & image success");
            return userDataForAdmin;
        });
    }

    public Page<UserDataForAdmin> searchListOfUserWithCondition(UserSearchInput userSearchInput, Pageable pageable) {
        Page<UserDataForSearching> users = userRepositoryMy.searchListOfUsersWithCondition(
                userSearchInput.getRole(),
                userSearchInput.getUsername(),
                userSearchInput.getEmail(), pageable);

        return users.map(user -> {
            log.info(user.getUserId(), user.getImageId());

            UserDataForAdmin userDataForUser = new UserDataForAdmin();
            userDataForUser.setId(user.getUserId());
            userDataForUser.setName(user.getUsername());
            userDataForUser.setBirthDate(user.getBirthDate());
            userDataForUser.setEmail(user.getEmail());
            userDataForUser.setImageId(user.getImageId());
            userDataForUser.setImageName(user.getImageName());
            userDataForUser.setImagePath(user.getImagePath());

            log.info("Search user success");
            return userDataForUser;
        });
    }

    public Page<UserDataForUser> searchListOfUser(UserSearchInput userSearchInput, Pageable pageable) {
        Page<UserDataForSearching> users = userRepositoryMy.searchListOfUsers(
                userSearchInput.getUsername(),
                userSearchInput.getEmail(), pageable);

        return users.map(user -> {
            log.info(user.getUserId(), user.getImageId());

            UserDataForUser userDataForUser = new UserDataForUser();
            userDataForUser.setId(user.getUserId());
            userDataForUser.setName(user.getUsername());
            userDataForUser.setBirthDate(user.getBirthDate());
            userDataForUser.setEmail(user.getEmail());
            userDataForUser.setImageId(user.getImageId());
            userDataForUser.setImageName(user.getImageName());
            userDataForUser.setImagePath(user.getImagePath());

            log.info("Search user success");
            return userDataForUser;
        });
    }
}
