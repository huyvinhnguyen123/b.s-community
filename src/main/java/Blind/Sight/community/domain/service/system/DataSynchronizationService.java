package Blind.Sight.community.domain.service.system;

import Blind.Sight.community.domain.entity.*;
import Blind.Sight.community.domain.entity.address.City;
import Blind.Sight.community.domain.entity.address.District;
import Blind.Sight.community.domain.entity.address.Ward;
import Blind.Sight.community.domain.entity.many.CategoryImage;
import Blind.Sight.community.domain.entity.many.ProductCategory;
import Blind.Sight.community.domain.entity.many.ProductImage;
import Blind.Sight.community.domain.entity.many.UserImage;
import Blind.Sight.community.domain.repository.mysql.*;
import Blind.Sight.community.domain.repository.postgresql.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class DataSynchronizationService {
    private final UserRepositoryPg userRepositoryPg;
    private final UserRepositoryMy userRepositoryMy;
    private final RoleRepositoryPg roleRepositoryPg;
    private final RoleRepositoryMy roleRepositoryMy;
    private final ImageRepositoryPg imageRepositoryPg;
    private final ImageRepositoryMy imageRepositoryMy;
    private final UserImageRepositoryPg userImageRepositoryPg;
    private final UserImageRepositoryMy userImageRepositoryMy;
    private final TypeRepositoryPg typeRepositoryPg;
    private final TypeRepositoryMy typeRepositoryMy;
    private final CityRepositoryPg cityRepositoryPg;
    private final CityRepositoryMy cityRepositoryMy;
    private final DistrictRepositoryPg districtRepositoryPg;
    private final DistrictRepositoryMy districtRepositoryMy;
    private final WardRepositoryPg wardRepositoryPg;
    private final WardRepositoryMy wardRepositoryMy;
    private final CategoryRepositoryPg categoryRepositoryPg;
    private final CategoryRepositoryMy categoryRepositoryMy;
    private final CategoryImageRepositoryPg categoryImageRepositoryPg;
    private final CategoryImageRepositoryMy categoryImageRepositoryMy;
    private final ProductRepositoryPg productRepositoryPg;
    private final ProductRepositoryMy productRepositoryMy;
    private final ProductImageRepositoryPg productImageRepositoryPg;
    private final ProductImageRepositoryMy productImageRepositoryMy;
    private final ProductCategoryRepositoryPg productCategoryRepositoryPg;
    private final ProductCategoryRepositoryMy productCategoryRepositoryMy;
//====================================================================
    // User
    public void synchronizeMySQLUserData() {
        List<User> users = userRepositoryPg.findAll();
        for(User user: users) {
            userRepositoryMy.save(user);
        }

        log.info("User's data has been transferred!");
    }

    public void synchronizeMySQLUserDataUpdate() {
        List<User> pgUsers = userRepositoryPg.findAll();
        List<User> myUsers = userRepositoryMy.findAll();

        // Create a map for efficient comparison based on user IDs.
        Map<String, User> pgUserMap = pgUsers.stream().collect(Collectors.toMap(User::getUserId, id -> id));

        for (User myUser : myUsers) {
            String userId = myUser.getUserId();
            User pgUser = pgUserMap.get(userId);

            if (pgUser == null) {
                // User exists in MySQL but not in Postgres; delete it.
                userRepositoryMy.delete(myUser);
            } else {
                // User exists in both MySQL and Postgres; update it.
                myUser.setUserName(pgUser.getUsername()); // Update fields as needed.
                myUser.setEmail(pgUser.getEmail()); // Update fields as needed.
                myUser.setBirthDate(pgUser.getBirthDate());
                myUser.setRoles(pgUser.getRoles());
                myUser.setUpdateAt(pgUser.getUpdateAt());
                userRepositoryMy.save(myUser);
            }
        }

        // Insert new records from Postgres.
        for (User pgUser : pgUsers) {
            String userId = pgUser.getUserId();
            if (userRepositoryMy.findById(userId).isEmpty()) {
                // User exists in Postgres but not in MySQL; insert it.
                userRepositoryMy.save(pgUser);
            }
        }

        log.info("User's data has been synchronized!");
    }
//====================================================================
    // Role
    public void synchronizeMySQLRoleData() {
        List<Role> roles = roleRepositoryPg.findAll();
        for(Role role: roles) {
            roleRepositoryMy.save(role);
        }

        log.info("Role's data has been transferred!");
    }

    public void synchronizeMySQLRoleDataUpdate() {
        List<Role> pgRoles = roleRepositoryPg.findAll();
        List<Role> myRoles = roleRepositoryMy.findAll();

        Map<Integer, Role> pgRoleMap = pgRoles.stream().collect(Collectors.toMap(Role::getRoleId, id -> id));

        for (Role myRole: myRoles) {
            Integer roleId = myRole.getRoleId();
            Role pgRole = pgRoleMap.get(roleId);

            if (pgRole == null) {
                roleRepositoryMy.delete(myRole);
            } else {
                myRole.setRoleName(pgRole.getRoleName());
                myRole.setDescription(pgRole.getDescription());
                myRole.setIsAdministration(pgRole.getIsAdministration());
                myRole.setUser(pgRole.getUser());
                myRole.setUpdateAt(pgRole.getUpdateAt());
                roleRepositoryMy.save(myRole);
            }
        }

        for (Role pgRole: pgRoles) {
            Integer roleId = pgRole.getRoleId();
            if (roleRepositoryMy.findById(roleId).isEmpty()) {
                roleRepositoryMy.save(pgRole);
            }
        }

        log.info("Role's data has been synchronized!");
    }
//====================================================================
    // Image
    public void synchronizeMySQLImageData() {
        List<Image> images = imageRepositoryPg.findAll();
        for(Image image: images) {
            imageRepositoryMy.save(image);
        }

        log.info("Image's data has been transferred!");
    }

    public void synchronizeMySQLImageDataUpdate() {
        List<Image> pgImages = imageRepositoryPg.findAll();
        List<Image> myImages = imageRepositoryMy.findAll();

        Map<String, Image> pgImageMap = pgImages.stream().collect(Collectors.toMap(Image::getImageId, id -> id));

        for (Image myImage: myImages) {
            String imageId = myImage.getImageId();
            Image pgImage = pgImageMap.get(imageId);

            if (pgImage == null) {
                imageRepositoryMy.delete(myImage);
            } else {
                myImage.setImageName(pgImage.getImageName());
                myImage.setImagePath(pgImage.getImagePath());
                myImage.setImagePathId(pgImage.getImagePathId());
                myImage.setTimeUpload(pgImage.getTimeUpload());
                myImage.setUpdateAt(pgImage.getUpdateAt());
                imageRepositoryMy.save(myImage);
            }

        }

        for (Image pgImage : pgImages) {
            String imageId = pgImage.getImageId();
            if (imageRepositoryMy.findById(imageId).isEmpty()) {
                imageRepositoryMy.save(pgImage);
            }
        }

        log.info("Image's data has been synchronized!");
    }
//====================================================================
    // UserImage
    public void synchronizeMySQLUserImageData() {
        List<UserImage> userImages = userImageRepositoryPg.findAll();
        for(UserImage userImage: userImages) {
            userImageRepositoryMy.save(userImage);
        }

        log.info("UserImage's data has been transferred!");
    }

    public void synchronizeMySQLUserImageDataUpdate() {
        List<UserImage> pgUserImages = userImageRepositoryPg.findAll();
        List<UserImage> myUserImages = userImageRepositoryMy.findAll();

        Map<Long, UserImage> pgUserImageMap = pgUserImages.stream().collect(Collectors.toMap(UserImage::getUserImageId, id -> id));

        for (UserImage myUserImage: myUserImages) {
            Long userImageId = myUserImage.getUserImageId();
            UserImage pgUserImage = pgUserImageMap.get(userImageId);

            if (pgUserImage == null) {
                userImageRepositoryMy.delete(myUserImage);
            } else {
                myUserImage.setUser(pgUserImage.getUser());
                myUserImage.setImage(pgUserImage.getImage());
                userImageRepositoryMy.save(myUserImage);
            }

        }

        for (UserImage pgUserImage : pgUserImages) {
            Long userImageId = pgUserImage.getUserImageId();
            if (userImageRepositoryMy.findById(userImageId).isEmpty()) {
                userImageRepositoryMy.save(pgUserImage);
            }
        }

        log.info("UserImage's data has been synchronized!");
    }
//====================================================================
    // Type
    public void synchronizeMySQLTypeData() {
        List<Type> types = typeRepositoryPg.findAll();
        for(Type type: types) {
            typeRepositoryMy.save(type);
        }

        log.info("Type's data has been transferred!");
    }

    public void synchronizeMySQLTypeDataUpdate() {
        List<Type> pgTypes = typeRepositoryPg.findAll();
        List<Type> myTypes = typeRepositoryMy.findAll();

        Map<String, Type> pgTypeMap = pgTypes.stream().collect(Collectors.toMap(Type::getTypeId, id -> id));

        for (Type myType: myTypes) {
            String typeId = myType.getTypeId();
            Type pgType = pgTypeMap.get(typeId);

            if (pgType == null) {
                typeRepositoryMy.delete(myType);
            } else {
                myType.setTypeName(pgType.getTypeName());
                myType.setUpdateAt(pgType.getUpdateAt());
                typeRepositoryMy.save(myType);
            }

        }

        for (Type pgType : pgTypes) {
            String typeId = pgType.getTypeId();
            if (typeRepositoryMy.findById(typeId).isEmpty()) {
                typeRepositoryMy.save(pgType);
            }
        }

        log.info("Type's data has been synchronized!");
    }
//====================================================================
    // City
    public void synchronizeMySQLCityData() {
        List<City> cities = cityRepositoryPg.findAll();
        for(City city: cities) {
            cityRepositoryMy.save(city);
        }

        log.info("City's data has been transferred!");
    }

    public void synchronizeMySQLCityDataUpdate() {
        List<City> pgCities = cityRepositoryPg.findAll();
        List<City> myCities = cityRepositoryMy.findAll();

        Map<Long, City> pgCityMap = pgCities.stream().collect(Collectors.toMap(City::getCityId, id -> id));

        for (City myCity: myCities) {
            Long cityId = myCity.getCityId();
            City pgCity = pgCityMap.get(cityId);

            if (pgCity == null) {
                cityRepositoryMy.delete(myCity);
            } else {
                myCity.setCityName(pgCity.getCityName());
                myCity.setUpdateAt(pgCity.getUpdateAt());
                cityRepositoryMy.save(myCity);
            }

        }

        for (City pgCity : pgCities) {
            Long cityId = pgCity.getCityId();
            if (cityRepositoryMy.findById(cityId).isEmpty()) {
                cityRepositoryMy.save(pgCity);
            }
        }

        log.info("City's data has been synchronized!");
    }

    // District
    public void synchronizeMySQLDistrictData() {
        List<District> districts = districtRepositoryPg.findAll();
        for(District district: districts) {
            districtRepositoryMy.save(district);
        }

        log.info("District's data has been transferred!");
    }

    public void synchronizeMySQLDistrictDataUpdate() {
        List<District> pgDistricts = districtRepositoryPg.findAll();
        List<District> myDistricts = districtRepositoryMy.findAll();

        Map<Long, District> pgDistrictMap = pgDistricts.stream().collect(Collectors.toMap(District::getDistrictId, id -> id));

        for (District myDistrict: myDistricts) {
            Long districtId = myDistrict.getDistrictId();
            District pgDistrict = pgDistrictMap.get(districtId);

            if (pgDistrict == null) {
                districtRepositoryMy.delete(myDistrict);
            } else {
                myDistrict.setDistrictName(pgDistrict.getDistrictName());
                myDistrict.setUpdateAt(pgDistrict.getUpdateAt());
                districtRepositoryMy.save(myDistrict);
            }

        }

        for (District pgDistrict : pgDistricts) {
            Long districtId = pgDistrict.getDistrictId();
            if (districtRepositoryMy.findById(districtId).isEmpty()) {
                districtRepositoryMy.save(pgDistrict);
            }
        }

        log.info("District's data has been synchronized!");
    }

    // Ward
    public void synchronizeMySQLWardData() {
        List<Ward> wards = wardRepositoryPg.findAll();
        for(Ward ward: wards) {
            wardRepositoryMy.save(ward);
        }

        log.info("Ward's data has been transferred!");
    }

    public void synchronizeMySQLWardDataUpdate() {
        List<Ward> pgWards = wardRepositoryPg.findAll();
        List<Ward> myWards = wardRepositoryMy.findAll();

        Map<Long, Ward> pgWardMap = pgWards.stream().collect(Collectors.toMap(Ward::getWardId, id -> id));

        for (Ward myWard: myWards) {
            Long wardId = myWard.getWardId();
            Ward pgWard = pgWardMap.get(wardId);

            if (pgWard == null) {
                wardRepositoryMy.delete(myWard);
            } else {
                myWard.setWardName(pgWard.getWardName());
                myWard.setUpdateAt(pgWard.getUpdateAt());
                wardRepositoryMy.save(myWard);
            }

        }

        for (Ward pgWard : pgWards) {
            Long wardId = pgWard.getWardId();
            if (wardRepositoryMy.findById(wardId).isEmpty()) {
                wardRepositoryMy.save(pgWard);
            }
        }

        log.info("Ward's data has been synchronized!");
    }
//====================================================================
    // Category
    public void synchronizeMySQLCategoryData() {
        List<Category> categories = categoryRepositoryPg.findAll();
        for(Category category: categories) {
            categoryRepositoryMy.save(category);
        }

        log.info("Category's data has been transferred!");
    }

    public void synchronizeMySQLCategoryDataUpdate() {
        List<Category> pgCategories = categoryRepositoryPg.findAll();
        List<Category> myCategories = categoryRepositoryMy.findAll();

        Map<String, Category> pgCategoryMap = pgCategories.stream().collect(Collectors.toMap(Category::getCategoryId, id -> id));

        for (Category myCategory: myCategories) {
            String categoryId = myCategory.getCategoryId();
            Category pgCategory = pgCategoryMap.get(categoryId);

            if (pgCategory == null) {
                categoryRepositoryMy.delete(myCategory);
            } else {
                myCategory.setCategoryName(pgCategory.getCategoryName());
                myCategory.setUpdateAt(pgCategory.getUpdateAt());
                categoryRepositoryMy.save(myCategory);
            }
        }

        log.info("Category's data has been synchronized!");
    }
//====================================================================
    // Category Image
    public void synchronizeMySQLCategoryImageData() {
        List<CategoryImage> categoryImages = categoryImageRepositoryPg.findAll();
        for (CategoryImage categoryImage: categoryImages) {
            categoryImageRepositoryMy.save(categoryImage);
        }

        log.info("Category Image's data has been transferred!");
    }

    public void synchronizeMySQLCategoryImageDataUpdate() {
        List<CategoryImage> pgCategoryImages = categoryImageRepositoryPg.findAll();
        List<CategoryImage> myCategoryImages = categoryImageRepositoryMy.findAll();

        Map<Long, CategoryImage> pgCategoryImageMap = pgCategoryImages.stream().collect(Collectors.toMap(CategoryImage::getCategoryImageId, id -> id));

        for (CategoryImage myCategoryImage : myCategoryImages) {
            Long categoryImageId = myCategoryImage.getCategoryImageId();
            CategoryImage pgCategoryImage = pgCategoryImageMap.get(categoryImageId);

            if(pgCategoryImage == null) {
                categoryImageRepositoryMy.delete(myCategoryImage);
            } else {
                myCategoryImage.setCategory(pgCategoryImage.getCategory());
                myCategoryImage.setImage(pgCategoryImage.getImage());
                categoryImageRepositoryMy.save(myCategoryImage);
            }
        }

        log.info("Category Image's data has been synchronized!");
    }
//====================================================================
    // Product
    public void synchronizeMySQLProductData() {
        List<Product> products = productRepositoryPg.findAll();
        for (Product product: products) {
            productRepositoryMy.save(product);
        }

        log.info("Product's data has been transferred!");
    }

    public void synchronizeMySQLProductDataUpdate() {
        List<Product> pgProducts = productRepositoryPg.findAll();
        List<Product> myProducts =  productRepositoryMy.findAll();

        Map<String, Product> pgProductMap = pgProducts.stream().collect(Collectors.toMap(Product::getProductId, id -> id));

        for (Product myProduct: myProducts) {
            String productId = myProduct.getProductId();
            Product pgProduct = pgProductMap.get(productId);

            if (pgProduct == null) {
                productRepositoryMy.delete(myProduct);
            } else {
                myProduct.setSku(pgProduct.getSku());
                myProduct.setName(pgProduct.getName());
                myProduct.setRegion(pgProduct.getRegion());
                myProduct.setMaterial(pgProduct.getMaterial());
                myProduct.setBrand(pgProduct.getBrand());
                myProduct.setPrice(pgProduct.getPrice());
                myProduct.setPoint(pgProduct.getPoint());
                myProduct.setDescription(pgProduct.getDescription());
                myProduct.setDatePost(pgProduct.getDatePost());
                myProduct.setColor(pgProduct.getColor());
                myProduct.setLength(pgProduct.getLength());
                myProduct.setWidth(pgProduct.getWidth());
                myProduct.setHeight(pgProduct.getHeight());
                myProduct.setWeight(pgProduct.getWeight());
                productRepositoryMy.save(myProduct);
            }
        }

        log.info("Product's data has been synchronized!");
    }
//====================================================================
    // Product Image
    public void synchronizeMySQLProductImageData() {
        List<ProductImage> productImages = productImageRepositoryPg.findAll();
        for (ProductImage productImage: productImages) {
            productImageRepositoryMy.save(productImage);
        }

        log.info("Product Image's data has been transferred!");
    }

    public void synchronizeMySQLProductImageDataUpdate() {
        List<ProductImage> pgProductImages = productImageRepositoryPg.findAll();
        List<ProductImage> myProductImages = productImageRepositoryMy.findAll();

        Map<Long, ProductImage> pgProductImageMap = pgProductImages.stream().collect(Collectors.toMap(ProductImage::getProductImageId, id -> id));

        for (ProductImage myProductImage: myProductImages) {
            Long productImageId = myProductImage.getProductImageId();
            ProductImage pgProductImage = pgProductImageMap.get(productImageId);

            if (pgProductImage == null) {
                productImageRepositoryMy.delete(myProductImage);
            } else {
                myProductImage.setProduct(pgProductImage.getProduct());
                myProductImage.setImage(pgProductImage.getImage());
                productImageRepositoryMy.save(myProductImage);
            }
        }

        log.info("Product Image's data has been synchronized!");
    }
//====================================================================
    // Product Category
    public void synchronizeMySQLProductCategoryData() {
        List<ProductCategory> productCategories = productCategoryRepositoryPg.findAll();
        for (ProductCategory productCategory : productCategories) {
            productCategoryRepositoryMy.save(productCategory);
        }

        log.info("Product Category's data has been transferred!");
    }

    public void synchronizeMySQLProductCategoryDataUpdate() {
        List<ProductCategory> pgProductCategories = productCategoryRepositoryPg.findAll();
        List<ProductCategory> myProductCategories = productCategoryRepositoryMy.findAll();

        Map<Long, ProductCategory> pgProductCategoryMap = pgProductCategories.stream().collect(Collectors.toMap(ProductCategory::getProductCategoryId, id -> id));

        for (ProductCategory myProductCategory : myProductCategories) {
            Long productCategory = myProductCategory.getProductCategoryId();
            ProductCategory pgProductCategory = pgProductCategoryMap.get(productCategory);

            if(pgProductCategory == null) {
                productCategoryRepositoryMy.delete(myProductCategory);
            } else {
                myProductCategory.setCategory(pgProductCategory.getCategory());
                myProductCategory.setProduct(pgProductCategory.getProduct());
                productCategoryRepositoryMy.save(myProductCategory);
            }
        }

        log.info("Product Category's data has been synchronized!");
    }
//====================================================================
}
