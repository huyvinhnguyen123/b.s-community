package Blind.Sight.community.domain.service.write;

import Blind.Sight.community.domain.entity.Permission;
import Blind.Sight.community.domain.entity.Role;
import Blind.Sight.community.domain.repository.postgresql.PermissionRepositoryPg;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class PermissionServicePg {
    private final PermissionRepositoryPg permissionRepositoryPg;

    public void createPermission(String serviceName, String description, Role role) {
        Permission permission = new Permission();
        permission.setPermissionName(serviceName);
        permission.setPageKey(serviceName.trim());
        permission.setDescription(description);
        permission.setRole(role);

        permissionRepositoryPg.save(permission);
        log.info("Set permission for role success");
    }
}
