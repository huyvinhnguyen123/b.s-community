package Blind.Sight.community.domain.service.read;

import Blind.Sight.community.domain.repository.mysql.ProductRepositoryMy;
import Blind.Sight.community.domain.repository.mysql.object.ProductDataForSearching;
import Blind.Sight.community.dto.category.CategoryData;
import Blind.Sight.community.dto.product.ProductData;
import Blind.Sight.community.dto.product.ProductSearchInput;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class ProductServiceMy {
    private final ProductRepositoryMy productRepositoryMy;

    public Page<ProductData> findAllProducts(Pageable pageable) {
        List<CategoryData> categoryDataList = new ArrayList<>();
        Page<ProductDataForSearching> products = productRepositoryMy.findAllProducts(pageable);
        return products.map(product -> {
                    ProductData productData = new ProductData();
                    productData.setProductId(product.getProductId());
                    productData.setSku(product.getSku());
                    productData.setName(product.getName());
                    productData.setPrice(product.getPrice());
                    productData.setPoint(product.getPoint());
                    productData.setDescription(product.getDescription());
                    productData.setDatePost(product.getDatePost());
                    productData.setLength(product.getLength());
                    productData.setWidth(product.getWidth());
                    productData.setHeight(product.getHeight());
                    productData.setWeight(product.getWeight());
                    productData.setImageId(product.getImageId());
                    productData.setImageName(product.getImageName());
                    productData.setImagePath(product.getImagePath());

                    productData.setCategoryId(product.getCategoryId());
                    productData.setCategoryName(product.getCategoryName());

//                    CategoryData categoryData = new CategoryData();
//                    categoryData.setCategoryId(product.getCategoryId());
//                    categoryData.setCategoryName(product.getCategoryName());
//                    categoryDataList.add(categoryData);
//                    productData.setCategoryData(categoryDataList);

                    log.info("Select products success");
                    return productData;
                });
    }

    public Page<ProductData> searchListOfProducts(ProductSearchInput productSearchInput, Pageable pageable) {
        Page<ProductDataForSearching> products = productRepositoryMy.searchListOfProducts(
                productSearchInput.getName(),
                productSearchInput.getFirstPrice(), productSearchInput.getSecondPrice(), productSearchInput.getPrice(),
                productSearchInput.getFirstPoint(), productSearchInput.getSecondPoint(), productSearchInput.getPoint(),
                productSearchInput.getFirstDate(), productSearchInput.getSecondDate(), productSearchInput.getDatePost(),
                productSearchInput.getFirstLength(), productSearchInput.getSecondLength(), productSearchInput.getLength(),
                productSearchInput.getFirstWidth(), productSearchInput.getSecondWidth(), productSearchInput.getWidth(),
                productSearchInput.getFirstHeight(), productSearchInput.getSecondHeight(), productSearchInput.getHeight(),
                productSearchInput.getFirstWeight(), productSearchInput.getSecondWeight(), productSearchInput.getWeight(),
                productSearchInput.getCategoryName(),
                pageable);

        return products.map(product -> {
            ProductData productData = new ProductData();
            productData.setProductId(product.getProductId());
            productData.setSku(product.getSku());
            productData.setName(product.getName());
            productData.setPrice(product.getPrice());
            productData.setPoint(product.getPoint());
            productData.setDescription(product.getDescription());
            productData.setDatePost(product.getDatePost());
            productData.setLength(product.getLength());
            productData.setWidth(product.getWidth());
            productData.setHeight(product.getHeight());
            productData.setWeight(product.getWeight());
            productData.setImageId(product.getImageId());
            productData.setImageName(product.getImageName());
            productData.setImagePath(product.getImagePath());
            productData.setCategoryId(product.getCategoryId());
            productData.setCategoryName(product.getCategoryName());

            log.info("Search product data success");
            return productData;
        });
    }
}
