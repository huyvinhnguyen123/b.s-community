package Blind.Sight.community.domain.service.read;

import Blind.Sight.community.domain.repository.mysql.CategoryRepositoryMy;
import Blind.Sight.community.domain.repository.mysql.object.CategoryForSelecting;
import Blind.Sight.community.dto.category.CategoryData;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class CategoryServiceMy {
    private final CategoryRepositoryMy categoryRepositoryMy;

    public List<CategoryData> selectListOfCategory() {
        List<CategoryForSelecting> categories = categoryRepositoryMy.findAllCategoriesWithImage();
        return categories
                .stream()
                .map(category -> {
                    CategoryData categoryData = new CategoryData();
                    categoryData.setCategoryId(category.getCategoryId());
                    categoryData.setCategoryName(category.getCategoryName());
                    categoryData.setImageId(category.getImageId());
                    categoryData.setImageName(category.getImageName());
                    categoryData.setImagePath(category.getImagePath());
                    return categoryData;
                })
                .collect(Collectors.toList());
    }

}
