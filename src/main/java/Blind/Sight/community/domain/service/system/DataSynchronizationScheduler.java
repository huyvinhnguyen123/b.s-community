package Blind.Sight.community.domain.service.system;

import Blind.Sight.community.domain.entity.*;
import Blind.Sight.community.domain.entity.address.City;
import Blind.Sight.community.domain.entity.address.District;
import Blind.Sight.community.domain.entity.address.Ward;
import Blind.Sight.community.domain.entity.many.CategoryImage;
import Blind.Sight.community.domain.entity.many.ProductCategory;
import Blind.Sight.community.domain.entity.many.ProductImage;
import Blind.Sight.community.domain.entity.many.UserImage;
import Blind.Sight.community.domain.repository.mysql.*;
import Blind.Sight.community.domain.repository.postgresql.*;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class DataSynchronizationScheduler {
    private final DataSynchronizationService dataSynchronizationService;

    @Scheduled(fixedDelay = 60000) // Synchronize every 1 minutes
    public void synchronizeData() {
        synchronizeAllData();
    }

    private final UserRepositoryPg userRepositoryPg;
    private final RoleRepositoryPg roleRepositoryPg;
    private final ImageRepositoryPg imageRepositoryPg;
    private final UserImageRepositoryPg userImageRepositoryPg;
    private final TypeRepositoryPg typeRepositoryPg;
    private final CityRepositoryPg cityRepositoryPg;
    private final DistrictRepositoryPg districtRepositoryPg;
    private final WardRepositoryPg wardRepositoryPg;
    private final CategoryRepositoryPg categoryRepositoryPg;
    private final CategoryImageRepositoryPg categoryImageRepositoryPg;
    private final ProductRepositoryPg productRepositoryPg;
    private final ProductImageRepositoryPg productImageRepositoryPg;
    private final ProductCategoryRepositoryPg productCategoryRepositoryPg;

    private final UserRepositoryMy userRepositoryMy;
    private final RoleRepositoryMy roleRepositoryMy;
    private final ImageRepositoryMy imageRepositoryMy;
    private final UserImageRepositoryMy userImageRepositoryMy;
    private final TypeRepositoryMy typeRepositoryMy;
    private final CityRepositoryMy cityRepositoryMy;
    private final DistrictRepositoryMy districtRepositoryMy;
    private final WardRepositoryMy wardRepositoryMy;
    private final CategoryRepositoryMy categoryRepositoryMy;
    private final CategoryImageRepositoryMy categoryImageRepositoryMy;
    private final ProductRepositoryMy productRepositoryMy;
    private final ProductImageRepositoryMy productImageRepositoryMy;
    private final ProductCategoryRepositoryMy productCategoryRepositoryMy;

    public void synchronizeAllData() {
        synchronizeCity();
        synchronizeDistrict();
        synchronizeWard();
        synchronizeType();
        synchronizeUser();
        synchronizeRole();
        synchronizeImage();
        synchronizeCategory();
        synchronizeProduct();
        synchronizeUserImage();
        synchronizeCategoryImage();
        synchronizeProductImage();
        synchronizeProductCategory();
    }

    private void synchronizeUser() {
        List<User> pgUsers = userRepositoryPg.findAll();
        List<User> myUsers = userRepositoryMy.findAll();

        if(myUsers.isEmpty()) {
            dataSynchronizationService.synchronizeMySQLUserData();
        }

        if(!myUsers.equals(pgUsers)) {
            dataSynchronizationService.synchronizeMySQLUserDataUpdate();
        }
    }

    private void synchronizeRole() {
        List<Role> pgRoles = roleRepositoryPg.findAll();
        List<Role> myRoles = roleRepositoryMy.findAll();

        if(myRoles.isEmpty()) {
            dataSynchronizationService.synchronizeMySQLRoleData();
        }

        if(!myRoles.equals(pgRoles)) {
            dataSynchronizationService.synchronizeMySQLRoleDataUpdate();
        }
    }

    private void synchronizeImage() {
        List<Image> pgImages = imageRepositoryPg.findAll();
        List<Image> myImages = imageRepositoryMy.findAll();

        if(myImages.isEmpty()) {
            dataSynchronizationService.synchronizeMySQLImageData();
        }

        if(!myImages.equals(pgImages)) {
            dataSynchronizationService.synchronizeMySQLImageDataUpdate();
        }
    }

    private void synchronizeUserImage() {
        List<UserImage> pgUserImages = userImageRepositoryPg.findAll();
        List<UserImage> myUserImages = userImageRepositoryMy.findAll();

        if(!myUserImages.equals(pgUserImages)) {
            dataSynchronizationService.synchronizeMySQLUserImageDataUpdate();
        }

        if(myUserImages.isEmpty()) {
            dataSynchronizationService.synchronizeMySQLUserImageData();
        }
    }

    private void synchronizeType() {
        List<Type> pgTypes = typeRepositoryPg.findAll();
        List<Type> myTypes = typeRepositoryMy.findAll();

        if(myTypes.isEmpty()) {
            dataSynchronizationService.synchronizeMySQLTypeData();
        }

        if(!myTypes.equals(pgTypes)) {
            dataSynchronizationService.synchronizeMySQLTypeDataUpdate();
        }
    }

    private void synchronizeCity() {
        List<City> pgCities = cityRepositoryPg.findAll();
        List<City> myCities = cityRepositoryMy.findAll();

        if(myCities.isEmpty()) {
            dataSynchronizationService.synchronizeMySQLCityData();
        }

        if(!myCities.equals(pgCities)) {
            dataSynchronizationService.synchronizeMySQLCityDataUpdate();
        }
    }

    private void synchronizeDistrict() {
        List<District> pgDistricts = districtRepositoryPg.findAll();
        List<District> myDistricts = districtRepositoryMy.findAll();

        if(myDistricts.isEmpty()) {
            dataSynchronizationService.synchronizeMySQLDistrictData();
        }

        if(!myDistricts.equals(pgDistricts)) {
            dataSynchronizationService.synchronizeMySQLDistrictDataUpdate();
        }
    }

    private void synchronizeWard() {
        List<Ward> pgWards = wardRepositoryPg.findAll();
        List<Ward> myWards = wardRepositoryMy.findAll();

        if(myWards.isEmpty()) {
            dataSynchronizationService.synchronizeMySQLWardData();
        }

        if(!myWards.equals(pgWards)) {
            dataSynchronizationService.synchronizeMySQLWardDataUpdate();
        }
    }

    private void synchronizeCategory() {
        List<Category> pgCategories = categoryRepositoryPg.findAll();
        List<Category> myCategories = categoryRepositoryMy.findAll();

        if(myCategories.isEmpty()) {
            dataSynchronizationService.synchronizeMySQLCategoryData();
        }

        if(!myCategories.equals(pgCategories)) {
            dataSynchronizationService.synchronizeMySQLCategoryDataUpdate();
        }
    }

    private void synchronizeCategoryImage() {
        List<CategoryImage> pgCategoryImages = categoryImageRepositoryPg.findAll();
        List<CategoryImage> myCategoryImages = categoryImageRepositoryMy.findAll();

        if(myCategoryImages.isEmpty()) {
            dataSynchronizationService.synchronizeMySQLCategoryImageData();
        }

        if(!myCategoryImages.equals(pgCategoryImages)) {
            dataSynchronizationService.synchronizeMySQLCategoryImageDataUpdate();
        }
    }

    private void synchronizeProduct() {
        List<Product> pgProducts = productRepositoryPg.findAll();
        List<Product> myProducts = productRepositoryMy.findAll();

        if(myProducts.isEmpty()) {
            dataSynchronizationService.synchronizeMySQLProductData();
        }

        if(!myProducts.equals(pgProducts)) {
            dataSynchronizationService.synchronizeMySQLProductDataUpdate();
        }
    }

    private void synchronizeProductImage() {
        List<ProductImage> pgProductImages = productImageRepositoryPg.findAll();
        List<ProductImage> myProductImages = productImageRepositoryMy.findAll();

        if(myProductImages.isEmpty()) {
            dataSynchronizationService.synchronizeMySQLProductImageData();
        }

        if(!myProductImages.equals(pgProductImages)) {
            dataSynchronizationService.synchronizeMySQLProductImageDataUpdate();
        }
    }

    private void synchronizeProductCategory() {
        List<ProductCategory> pgProductCategories = productCategoryRepositoryPg.findAll();
        List<ProductCategory> myProductCategories = productCategoryRepositoryMy.findAll();

        if(myProductCategories.isEmpty()) {
            dataSynchronizationService.synchronizeMySQLProductCategoryData();
        }

        if(!myProductCategories.equals(pgProductCategories)) {
            dataSynchronizationService.synchronizeMySQLProductCategoryDataUpdate();
        }
    }
}
