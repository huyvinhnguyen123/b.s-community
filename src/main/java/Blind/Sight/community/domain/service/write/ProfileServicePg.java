package Blind.Sight.community.domain.service.write;

import Blind.Sight.community.domain.entity.Point;
import Blind.Sight.community.domain.entity.Profile;
import Blind.Sight.community.domain.entity.User;
import Blind.Sight.community.domain.entity.address.Address;
import Blind.Sight.community.domain.repository.postgresql.*;
import Blind.Sight.community.domain.repository.postgresql.object.ProfileDataForSelect;
import Blind.Sight.community.domain.service.system.AddressService;
import Blind.Sight.community.domain.service.system.PointService;
import Blind.Sight.community.dto.profile.ProfileInput;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@Slf4j
@RequiredArgsConstructor
public class ProfileServicePg {
    private final ProfileRepositoryPg profileRepositoryPg;
    private final PointRepositoryPg pointRepositoryPg;
    private final AddressRepositoryPg addressRepositoryPg;
    private final AddressService addressService;
    private final PointService pointService;

    public Profile findProfileById(String profileId) {
        Profile existProfile = profileRepositoryPg.findById(profileId).orElseThrow(
                () ->  new NullPointerException("Not found this profile: " + profileId)
        );

        log.info("Found this profile");
        return existProfile;
    }

    public ProfileDataForSelect findProfileByUserId(String userId) {
        ProfileDataForSelect existProfile = profileRepositoryPg.findByUserId(userId).orElseThrow(
                () -> new NullPointerException("Not found this user: " + userId)
        );

        log.info("Found this profile");
        return existProfile;
    }

    public void createProfile(Authentication authentication) {
        if (authentication != null && authentication.isAuthenticated()) {
            User userLogin = (User) authentication.getPrincipal();

            Profile profile = new Profile();
            profile.setUser(userLogin);
            profile.setTotalPoint(0.0);
            profileRepositoryPg.save(profile);
            log.info("Save profile success");

            Point point = pointService.createDefaultPoint(profile);

            profile.setTotalPoint(point.getPointExchange());
            profileRepositoryPg.save(profile);
            log.info("Update profile success");

            log.info("Create profile success");

        } else {
            log.error("Authentication failed");
        }
    }

    public void updateProfile(Authentication authentication, ProfileInput profileInput) {
        if (authentication != null && authentication.isAuthenticated()) {
            User userLogin = (User) authentication.getPrincipal();
            log.info(userLogin.getUsername());

            ProfileDataForSelect profileDataForSelect = findProfileByUserId(userLogin.getUserId());
            Profile existProfile = findProfileById(profileDataForSelect.getProfileId());
            existProfile.setPhone(profileInput.getPhone());
            existProfile.setUpdateAt(LocalDate.now());

            Address address = new Address();
            address.setProfile(existProfile);
            address.setAddressName(profileInput.getAddressName());
            address.setCity(addressService.findCityById(profileInput.getCityId()));
            address.setDistrict(addressService.findDistrictById(profileInput.getDistrictId()));
            address.setWard(addressService.findWardById(profileInput.getWardId()));

            addressRepositoryPg.save(address);
            log.info("Create address success");

            profileRepositoryPg.save(existProfile);
            log.info("Update profile success");
        }
    }

    public void updateProfileOnlyPoint(Authentication authentication) {
        if (authentication != null && authentication.isAuthenticated()) {
            User userLogin = (User) authentication.getPrincipal();

            ProfileDataForSelect profileDataForSelect = findProfileByUserId(userLogin.getUserId());
            Profile existProfile = findProfileById(profileDataForSelect.getProfileId());
            existProfile.setTotalPoint(222222.222);
            existProfile.setUpdateAt(LocalDate.now());

            profileRepositoryPg.save(existProfile);
            log.info("Update profile success");
        }
    }
}
