package Blind.Sight.community.domain.repository.mysql.object;

import java.time.LocalDate;

public interface UserDataForSearching {
    String getUserId();
    String getUsername();
    LocalDate getBirthDate();
    String getEmail();
    String getRole();
    String getImageId();
    String getImageName();
    String getImagePath();
}
