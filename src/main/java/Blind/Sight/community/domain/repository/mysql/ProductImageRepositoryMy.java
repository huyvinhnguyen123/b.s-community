package Blind.Sight.community.domain.repository.mysql;

import Blind.Sight.community.domain.entity.many.ProductImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductImageRepositoryMy extends JpaRepository<ProductImage, Long> {
}
