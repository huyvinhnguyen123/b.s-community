package Blind.Sight.community.domain.repository.mysql;

import Blind.Sight.community.domain.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepositoryMy extends JpaRepository<Role, Integer> {
}
