package Blind.Sight.community.domain.repository.mysql.object;

public interface CategoryForSelecting {
    String getCategoryId();
    String getCategoryName();
    String getImageId();
    String getImageName();
    String getImagePath();
}
