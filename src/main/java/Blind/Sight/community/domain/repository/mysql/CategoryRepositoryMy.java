package Blind.Sight.community.domain.repository.mysql;

import Blind.Sight.community.domain.entity.Category;
import Blind.Sight.community.domain.repository.mysql.object.CategoryForSelecting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepositoryMy extends JpaRepository<Category, String> {
    // for admin
    @Query(value = """
                SELECT c.categoryId, c.categoryName
                FROM Categories c
            """, nativeQuery = true)
    List<CategoryForSelecting> findAllCategories();

    // for admin & user
    @Query(value = """
                SELECT c.categoryId, c.categoryName, i.imageId, i.imageName, i.imagePath
                FROM Categories c
                LEFT JOIN Category_Images ci ON c.categoryId = ci.categoryId
                LEFT JOIN Images i ON i.imageId = ci.imageId
            """, nativeQuery = true)
    List<CategoryForSelecting> findAllCategoriesWithImage();
}
