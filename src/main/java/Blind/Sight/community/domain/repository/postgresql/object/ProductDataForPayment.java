package Blind.Sight.community.domain.repository.postgresql.object;

public interface ProductDataForPayment {
    String getProductId();
    String getName();
    Double getPrice();
    Double getPoint();
}
