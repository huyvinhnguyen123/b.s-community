package Blind.Sight.community.domain.repository.mysql;

import Blind.Sight.community.domain.entity.many.CategoryImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryImageRepositoryMy extends JpaRepository<CategoryImage, Long> {
}
