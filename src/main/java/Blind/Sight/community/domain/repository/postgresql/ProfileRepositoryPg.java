package Blind.Sight.community.domain.repository.postgresql;

import Blind.Sight.community.domain.entity.Profile;
import Blind.Sight.community.domain.repository.postgresql.object.ProfileDataForSelect;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProfileRepositoryPg extends JpaRepository<Profile, String> {
    @Query(value = """
            SELECT p.profileId, p.totalPoint, p.phone, p.dateJoin,
            u.userId, u.userName, u.email, u.birthDate
            FROM Profiles p
            LEFT JOIN Users u ON u.userId = p.userId
            WHERE u.userid = ?1
            """, nativeQuery = true)
    Optional<ProfileDataForSelect> findByUserId(String userId);
}
