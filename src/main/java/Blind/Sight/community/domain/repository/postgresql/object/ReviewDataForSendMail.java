package Blind.Sight.community.domain.repository.postgresql.object;

public interface ReviewDataForSendMail {
    String getProductId();
    String getUserId();
}
