package Blind.Sight.community.domain.repository.mysql.object;

import java.time.LocalDate;

public interface ProductDataForSearching {
    String getProductId();
    String getSku();
    String getName();
    Double getPrice();
    Double getPoint();
    String getDescription();
    LocalDate getDatePost();
    Double getLength();
    Double getWidth();
    Double getHeight();
    Double getWeight();
    String getImageId();
    String getImageName();
    String getImagePath();
    String getCategoryId();
    String getCategoryName();
}
