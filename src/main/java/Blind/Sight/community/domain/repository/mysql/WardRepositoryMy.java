package Blind.Sight.community.domain.repository.mysql;

import Blind.Sight.community.domain.entity.address.Ward;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WardRepositoryMy extends JpaRepository<Ward, Long> {
}
