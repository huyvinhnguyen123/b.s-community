package Blind.Sight.community.domain.repository.mysql;

import Blind.Sight.community.domain.entity.Product;
import Blind.Sight.community.domain.repository.mysql.object.ProductDataForSearching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface ProductRepositoryMy extends JpaRepository<Product, String> {
    @Query(value = """
                SELECT p.productId, p.sku, p.name, p.material, p.region, p.brand, p.price, p.point, p.description, p.datePost,
                p.color, p.length, p.width, p.height, p.weight,
                i.imageId, i.imageName, i.imagePath,
                c.categoryId, c.categoryName
                FROM Products p
                LEFT JOIN Product_Images pi ON p.productId = pi.productId
                LEFT JOIN Images i ON i.imageId = pi.imageId
                LEFT JOIN Product_Categories pc ON pc.productId = p.productId
                LEFT JOIN Categories c ON c.categoryId = pc.categoryId
            """, nativeQuery = true)
    Page<ProductDataForSearching> findAllProducts(Pageable pageable);

    @Query(value = """
                SELECT p.productId, p.sku, p.name, p.price, p.point, p.description, p.datePost,
                p.length, p.width, p.height, p.weight,
                i.imageId, i.imageName, i.imagePath,
                c.categoryId, c.categoryName
                FROM Products p
                LEFT JOIN Product_Images pi ON p.productId = pi.productId
                LEFT JOIN Images i ON i.imageId = pi.imageId
                LEFT JOIN Product_Categories pc ON pc.productId = p.productId
                LEFT JOIN Categories c ON c.categoryId = pc.categoryId
                WHERE (p.name IS NULL OR p.name LIKE %:name%)
                OR (p.price IS NULL OR (p.price BETWEEN :firstPrice AND :secondPrice OR p.price = :price))
                OR (p.point IS NULL OR (p.point BETWEEN :firstPoint AND :secondPoint OR p.point = :point))
                OR (p.datePost BETWEEN :firstDate AND :secondDate OR p.datePost = :datePost)
                OR (p.length IS NULL OR (p.length BETWEEN :firstLength AND :secondLength OR p.length = :length))
                OR (p.width IS NULL OR (p.width BETWEEN :firstWidth AND :secondWidth OR p.width = :width))
                OR (p.height IS NULL OR (p.height BETWEEN :firstHeight AND :secondHeight OR p.height = :height))
                OR (p.weight IS NULL OR (p.weight BETWEEN :firstWeight AND :secondWeight OR p.weight = :weight))
                OR c.categoryName IS NULL OR c.categoryName IN (
                    SELECT c.categoryName
                    FROM Categories c
                    LEFT JOIN Category_Images ci ON c.categoryId = ci.categoryId
                    LEFT JOIN Images i ON i.imageId = ci.imageId
                    WHERE categoryName LIKE %:categoryName%
                )
            """, nativeQuery = true)
    Page<ProductDataForSearching> searchListOfProducts(@Param("name") String name,
                                                       @Param("firstPrice") Double firstPrice,
                                                       @Param("secondPrice") Double secondPrice,
                                                       @Param("price") Double price,
                                                       @Param("firstPoint") Double firstPoint,
                                                       @Param("secondPoint") Double secondPoint,
                                                       @Param("point") Double point,
                                                       @Param("firstDate") LocalDate firstDate,
                                                       @Param("secondDate") LocalDate secondDate,
                                                       @Param("datePost") LocalDate datePost,
                                                       @Param("firstLength") Integer firstLength,
                                                       @Param("secondLength") Integer secondLength,
                                                       @Param("length") Integer length,
                                                       @Param("firstWidth") Integer firstWidth,
                                                       @Param("secondWidth") Integer secondWidth,
                                                       @Param("width") Integer width,
                                                       @Param("firstHeight") Integer firstHeight,
                                                       @Param("secondHeight") Integer secondHeight,
                                                       @Param("height") Integer height,
                                                       @Param("firstWeight") Integer firstWeight,
                                                       @Param("secondWeight") Integer secondWeight,
                                                       @Param("weight") Integer weight,
                                                       @Param("categoryName") String categoryName,
                                                       Pageable pageable);
}
