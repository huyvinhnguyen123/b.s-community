package Blind.Sight.community.domain.repository.postgresql;

import Blind.Sight.community.domain.entity.Permission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PermissionRepositoryPg extends JpaRepository<Permission, Integer> {
}
