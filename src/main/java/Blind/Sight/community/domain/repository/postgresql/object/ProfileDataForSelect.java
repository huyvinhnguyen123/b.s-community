package Blind.Sight.community.domain.repository.postgresql.object;

import java.time.LocalDate;

public interface ProfileDataForSelect {
    String getProfileId();
    Double getTotalPoint();
    String getPhone();
    LocalDate getDateJoin();
    String getUserId();
    String getUserName();
    String getEmail();
    LocalDate getBirthDate();
}
