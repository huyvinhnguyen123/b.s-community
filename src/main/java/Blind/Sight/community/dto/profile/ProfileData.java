package Blind.Sight.community.dto.profile;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ProfileData {
    private String profileId;
    private Double totalPoint;
    private String phone;
    private LocalDate dateJoin;
    private String userId;
    private String userName;
    private String email;
    private LocalDate birthDate;
}
