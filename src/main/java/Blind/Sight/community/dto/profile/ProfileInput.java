package Blind.Sight.community.dto.profile;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProfileInput {
    @Schema(example = "093-555-2222")
    private String phone;
    @Schema(example = "109/10 Le Loi Street, Ward 4,Go Vap District")
    private String addressName;
    @Schema(example = "2")
    private Long cityId;
    @Schema(example = "5")
    private Long districtId;
    @Schema(example = "14")
    private Long wardId;
}

