package Blind.Sight.community.dto.category;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoryData {
    private String categoryId;
    private String categoryName;
    private String imageId;
    private String imageName;
    private String imagePath;
}
