package Blind.Sight.community.dto.user;

import Blind.Sight.community.dto.validate.password.ValidPassword;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginInput {
    @NotNull(message = "{User.loginId.notNull}")
    @NotEmpty(message = "{User.loginId.notEmpty}")
    @Schema(example = "huy.vinhnguyen@outlook.com")
    @Email
    private String email;
    @NotNull(message = "{User.password.notNull}")
    @NotEmpty(message = "{User.password.notEmpty}")
    @Schema(example = "huyVN1234")
    @ValidPassword
    private String password;

    public LoginInput() {}
    public LoginInput(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
