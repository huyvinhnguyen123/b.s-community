package Blind.Sight.community.dto.product;

import Blind.Sight.community.dto.category.CategoryData;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Getter
@Setter
public class ProductInput {
    private String name;
    private String material;
    private String region;
    private String brand;
    private Double price;
    private String description;
    private String color;
    private Double length;
    private Double width;
    private Double height;
    private Double weight;
    private List<CategoryData> categoryData;
    private String categoryId;
    private String categoryName;
    private Long productCategoryId;
    private MultipartFile file;
    private String fileId;
}
