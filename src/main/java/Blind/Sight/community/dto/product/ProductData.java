package Blind.Sight.community.dto.product;

import Blind.Sight.community.domain.entity.Category;
import Blind.Sight.community.dto.category.CategoryData;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
public class ProductData {
    private String productId;
    private String sku;
    private String name;
    private Double price;
    private Double point;
    private String description;
    private LocalDate datePost;
    private Double length;
    private Double width;
    private Double height;
    private Double weight;
    private String imageId;
    private String imageName;
    private String imagePath;
    private List<CategoryData> categoryData;
    private String categoryId;
    private String categoryName;
}
