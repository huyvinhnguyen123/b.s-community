package Blind.Sight.community.dto.product;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ProductSearchInput {
    private String name;
    private Double firstPrice;
    private Double secondPrice;
    private Double price;
    private Double firstPoint;
    private Double secondPoint;
    private Double point;
    private String description;
    private LocalDate firstDate;
    private LocalDate secondDate;
    private LocalDate datePost;
    private Integer firstLength;
    private Integer secondLength;
    private Integer length;
    private Integer firstWidth;
    private Integer secondWidth;
    private Integer width;
    private Integer firstHeight;
    private Integer secondHeight;
    private Integer height;
    private Integer firstWeight;
    private Integer secondWeight;
    private Integer weight;
    private String categoryName;

}
