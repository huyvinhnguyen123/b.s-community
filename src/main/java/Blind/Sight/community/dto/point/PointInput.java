package Blind.Sight.community.dto.point;

import Blind.Sight.community.domain.entity.Profile;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PointInput {
    private Profile profile;
    private Double pointExchange;
    private Double decimalPlace;
    private Double exchangeValue;
}
