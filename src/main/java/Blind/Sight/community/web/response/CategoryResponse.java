package Blind.Sight.community.web.response;

import Blind.Sight.community.dto.category.CategoryData;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CategoryResponse {
    private Iterable<CategoryData> categories;
}
