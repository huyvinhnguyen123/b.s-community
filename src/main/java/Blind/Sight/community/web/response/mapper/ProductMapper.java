package Blind.Sight.community.web.response.mapper;

import Blind.Sight.community.dto.common.Pages;
import Blind.Sight.community.dto.product.ProductData;
import Blind.Sight.community.web.response.ProductResponse;
import org.springframework.data.domain.Pageable;

public class ProductMapper {
    private ProductMapper() {}

    public static ProductResponse mapToProduct(Iterable<ProductData> productDataList, Pageable pageable, Long totalProducts, Integer totalPages) {
        Pages pages = new Pages();
        pages.setNumber(pageable.getPageNumber());
        pages.setSize(pageable.getPageSize());
        pages.setSort(pageable.getSort());

        return ProductResponse.builder()
                .products(productDataList)
                .pages(pages)
                .totalProducts(totalProducts)
                .totalPages(totalPages)
                .build();

    }
}
