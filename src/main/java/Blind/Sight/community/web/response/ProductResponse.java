package Blind.Sight.community.web.response;

import Blind.Sight.community.dto.common.Pages;
import Blind.Sight.community.dto.product.ProductData;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ProductResponse {
    private Iterable<ProductData> products;
    private Pages pages;
    private Long totalProducts;
    private Integer totalPages;
}
