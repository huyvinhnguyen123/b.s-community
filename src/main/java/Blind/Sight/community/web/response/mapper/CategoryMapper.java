package Blind.Sight.community.web.response.mapper;

import Blind.Sight.community.dto.category.CategoryData;
import Blind.Sight.community.web.response.CategoryResponse;

public class CategoryMapper {
    private CategoryMapper() {}

    public static CategoryResponse mapToCategory(Iterable<CategoryData> categoryDataList) {
        return CategoryResponse.builder()
                .categories(categoryDataList)
                .build();
    }
}
