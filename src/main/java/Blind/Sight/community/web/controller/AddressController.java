package Blind.Sight.community.web.controller;

import Blind.Sight.community.domain.entity.address.City;
import Blind.Sight.community.domain.entity.address.District;
import Blind.Sight.community.domain.entity.address.Ward;
import Blind.Sight.community.domain.service.system.AddressService;
import Blind.Sight.community.web.response.common.ResponseDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("api/v1")
public class AddressController {
    private final AddressService addressService;

    @GetMapping("/cities")
    public ResponseEntity<ResponseDto<Object>> listAllCities() {
        Iterable<City> cities = addressService.findAllCities();
        return ResponseEntity.ok(ResponseDto.build().withData(cities));
    }

    @GetMapping("/districts")
    public ResponseEntity<ResponseDto<Object>> listAllDistricts() {
        Iterable<District> districts = addressService.findAllDistricts();
        return ResponseEntity.ok(ResponseDto.build().withData(districts));
    }

    @GetMapping("/wards")
    public ResponseEntity<ResponseDto<Object>> listAllWards() {
        Iterable<Ward> wards = addressService.findAllWards();
        return ResponseEntity.ok(ResponseDto.build().withData(wards));
    }
}
