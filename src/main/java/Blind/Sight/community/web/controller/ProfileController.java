package Blind.Sight.community.web.controller;

import Blind.Sight.community.domain.service.write.ProfileServicePg;
import Blind.Sight.community.dto.profile.ProfileInput;
import Blind.Sight.community.web.response.common.ResponseDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("api/v1/users")
public class ProfileController {
    private final ProfileServicePg profileServicePg;

    @PostMapping("/create-profile")
    public ResponseEntity<ResponseDto<Object>> createProfile(Authentication authentication) {
        log.info("Request updating user...");
        profileServicePg.createProfile(authentication);
        return ResponseEntity.ok(ResponseDto.build().withMessage("OK"));
    }

    @PutMapping("/update-profile")
    public ResponseEntity<ResponseDto<Object>> updateProfile(Authentication authentication, @RequestBody ProfileInput profileInput) {
        log.info("Request updating user...");
        profileServicePg.updateProfile(authentication, profileInput);
        return ResponseEntity.ok(ResponseDto.build().withMessage("OK"));
    }
}
