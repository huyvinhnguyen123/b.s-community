package Blind.Sight.community.web.controller;

import Blind.Sight.community.domain.service.read.ProductServiceMy;
import Blind.Sight.community.domain.service.write.ProductServicePg;
import Blind.Sight.community.dto.product.ProductData;
import Blind.Sight.community.dto.product.ProductInput;
import Blind.Sight.community.dto.product.ProductSearchInput;
import Blind.Sight.community.dto.user.UserData;
import Blind.Sight.community.web.response.ProductResponse;
import Blind.Sight.community.web.response.UserResponse;
import Blind.Sight.community.web.response.common.ResponseDto;
import Blind.Sight.community.web.response.mapper.ProductMapper;
import Blind.Sight.community.web.response.mapper.UserMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.GeneralSecurityException;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("api/v1")
public class ProductController {
    private final ProductServicePg productServicePg;
    private final ProductServiceMy productServiceMy;

    @PostMapping("/products/create")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ResponseDto<Object>> createProduct(@RequestBody @ModelAttribute ProductInput productInput) throws GeneralSecurityException, IOException {
        log.info("Request creating product...");
        productServicePg.createProduct(productInput);
        return ResponseEntity.ok(ResponseDto.build().withMessage("OK"));
    }

    @PutMapping("/products/update")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ResponseDto<Object>> updateProduct(@RequestParam String productId,
                                                             @RequestBody @ModelAttribute ProductInput productInput) throws GeneralSecurityException, IOException {
        log.info("Request updating product...");
        productServicePg.updateProduct(productId,productInput);
        return ResponseEntity.ok(ResponseDto.build().withMessage("OK"));
    }

    @DeleteMapping("/products/delete")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ResponseDto<Object>> deleteProduct(@RequestParam String productId) {
        log.info("Request deleting product...");
        productServicePg.deleteProduct(productId);
        return ResponseEntity.ok(ResponseDto.build().withMessage("OK"));
    }

    @DeleteMapping("/products/delete-all-relations")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ResponseDto<Object>> deleteProductAndProductRelation(@RequestParam String productId, @RequestParam String imageId,
                                                                               @RequestParam Long productImageId, @RequestParam  Long productCategoryId) throws GeneralSecurityException, IOException {
        log.info("Request deleting product...");
        productServicePg.deleteProductAndProductRelation(productId, imageId, productImageId, productCategoryId);
        return ResponseEntity.ok(ResponseDto.build().withMessage("OK"));
    }

    @GetMapping("/products")
    public ResponseEntity<ResponseDto<Object>> findAllProducts(@RequestParam(defaultValue = "0") int page,
                                                               @RequestParam(defaultValue = "1") int size) {
        log.info("Request selecting all products...");
        Pageable pageable = PageRequest.of(page, size, Sort.by("name").ascending());

        Page<ProductData> productDataPage = productServiceMy.findAllProducts(pageable);
        Long totalProducts = productDataPage.getTotalElements();
        Integer totalPages = productDataPage.getTotalPages();
        Iterable<ProductData> productDataList = productDataPage.getContent();

        ProductResponse productResponse = ProductMapper.mapToProduct(productDataList,pageable,totalProducts,totalPages);
        return ResponseEntity.ok(ResponseDto.build().withData(productResponse));
    }

    @GetMapping("/products/search")
    public ResponseEntity<ResponseDto<Object>> searchListOfProducts(@RequestParam(defaultValue = "0") int page,
                                                                    @RequestParam(defaultValue = "1") int size,
                                                                    @RequestBody @ModelAttribute ProductSearchInput productSearchInput) {
        log.info("Request selecting all products...");
        Pageable pageable = PageRequest.of(page, size, Sort.by("name").ascending());

        Page<ProductData> productDataPage = productServiceMy.searchListOfProducts(productSearchInput, pageable);
        Long totalProducts = productDataPage.getTotalElements();
        Integer totalPages = productDataPage.getTotalPages();
        Iterable<ProductData> productDataList = productDataPage.getContent();

        ProductResponse productResponse = ProductMapper.mapToProduct(productDataList,pageable,totalProducts,totalPages);
        return ResponseEntity.ok(ResponseDto.build().withData(productResponse));
    }
}
