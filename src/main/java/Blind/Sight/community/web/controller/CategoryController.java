package Blind.Sight.community.web.controller;

import Blind.Sight.community.domain.service.read.CategoryServiceMy;
import Blind.Sight.community.domain.service.write.CategoryServicePg;
import Blind.Sight.community.dto.category.CategoryData;
import Blind.Sight.community.dto.category.CategoryInput;
import Blind.Sight.community.web.response.CategoryResponse;
import Blind.Sight.community.web.response.common.ResponseDto;
import Blind.Sight.community.web.response.mapper.CategoryMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("api/v1")
public class CategoryController {
    private final CategoryServicePg categoryServicePg;
    private final CategoryServiceMy categoryServiceMy;

    @PostMapping("/categories/create")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ResponseDto<Object>> createCategory(@RequestBody @ModelAttribute CategoryInput categoryInput) throws GeneralSecurityException, IOException {
        log.info("Request creating category...");
        categoryServicePg.createCategory(categoryInput);
        return ResponseEntity.ok(ResponseDto.build().withMessage("OK"));
    }

    @PutMapping("/categories/update")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ResponseDto<Object>> updateCategory(@RequestParam String categoryId,
                                                              @RequestBody @ModelAttribute CategoryInput categoryInput) throws GeneralSecurityException, IOException {
        log.info("Request updating category...");
        categoryServicePg.updateCategory(categoryId, categoryInput);
        return ResponseEntity.ok(ResponseDto.build().withMessage("OK"));
    }

    @DeleteMapping("/categories/delete")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ResponseDto<Object>> deleteCategory(@RequestParam String categoryId, @RequestParam String imageId,
                                                              @RequestParam Long categoryImageId) throws GeneralSecurityException, IOException {
        log.info("Request deleting category...");
        categoryServicePg.deleteCategory(categoryId, imageId, categoryImageId);
        return ResponseEntity.ok(ResponseDto.build().withMessage("OK"));
    }

    @GetMapping("/categories")
    public ResponseEntity<ResponseDto<Object>> selectCategories() {
        log.info("Request selecting categories...");
        List<CategoryData> categoryData = categoryServiceMy.selectListOfCategory();
        CategoryResponse categoryResponse = CategoryMapper.mapToCategory(categoryData);
        return ResponseEntity.ok(ResponseDto.build().withData(categoryResponse));
    }
}
