package Blind.Sight.community.web.controller;

import Blind.Sight.community.domain.service.system.DataSynchronizationScheduler;
import Blind.Sight.community.web.response.common.ResponseDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("api/v1")
public class DataSynchronizeController {
    private final DataSynchronizationScheduler dataSynchronizationScheduler;

    @PostMapping("/synchronize-data")
    public ResponseEntity<ResponseDto<Object>> synchronizeAllData() {
        log.info("Request synchronizing data...");
        dataSynchronizationScheduler.synchronizeData();
        return ResponseEntity.ok(ResponseDto.build().withMessage("OK"));
    }
}
