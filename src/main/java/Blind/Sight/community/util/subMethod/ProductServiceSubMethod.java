package Blind.Sight.community.util.subMethod;

public class ProductServiceSubMethod {
    private static final Double SERVER_DEFAULT_EXCHANGE_RATE = 24290.50;

    private ProductServiceSubMethod() {}

    /**
     * Calculate point base on price (1 point = 1 USD)
     *
     * @param price - get price from product
     * @return - point
     */
    public static Double calculatePoint(Double price) {
        double point = (price * 1) / SERVER_DEFAULT_EXCHANGE_RATE;
        return (double) Math.round(point * 100) / 100;
    }

    /**
     * Using attribute to build sku base on name-region-material-color
     *
     * @param name - get name from product
     * @param region - get region from product
     * @param material - get material from product
     * @param color - get material from color
     * @return - sku
     */
    public static String skuBuilder(String name, String region, String material, String color) {
        StringBuilder sku = new StringBuilder();
        sku.append(name,0,3);
        sku.append(region,0,2);
        sku.append(material,0,3);
        sku.append(color,0,3);
        return sku.toString();
    }
}
